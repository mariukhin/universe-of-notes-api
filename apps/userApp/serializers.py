from django.core.validators import RegexValidator, EmailValidator, MinLengthValidator, MaxLengthValidator
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueValidator

from userApp.utils import decode_header
from .models import User
from .mixins import SetCustomErrorMessagesMixin

class RegistrationSerializer(SetCustomErrorMessagesMixin, serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(max_length=30, min_length=8, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'token')
        custom_error_messages = {
            'username': {
                UniqueValidator: _('This username is already taken. Please, try again'),
                RegexValidator: _('Invalid username'),
                MaxLengthValidator: _('Username must be no more than 50 symbols')
            },
            'email': {
                UniqueValidator: _('This email is already taken. Please, try again'),
                EmailValidator: _('Invalid email'),
                MaxLengthValidator: _('Email must be no more than 50 symbols')
            },
            'password': {
                MaxLengthValidator: _('Password must be no more than 30 symbols'),
                MinLengthValidator: _('Password must be more than 8 symbols'),
            },
        }

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

class LoginSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    email = serializers.CharField(read_only=True)
    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=30, min_length=8, write_only=True)
    rating = serializers.FloatField(required=False)

    # Ignore these fields if they are included in the request.
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'rating', 'token')


    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password', None)

        filteredByUsername = User.objects.filter(username=username)
        filteredByEmail = User.objects.filter(email=username)

        if filteredByUsername:
            user = filteredByUsername[0]
        elif filteredByEmail:
            user = filteredByEmail[0]
        else:
            raise serializers.ValidationError({'username': 'Invalid username!'})     

        if user.check_password(password):
            return user
        else:
            raise serializers.ValidationError({'password': 'Invalid password!'})

class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(max_length=50, required=False)
    email = serializers.EmailField(max_length=50, required=False)
    password = serializers.CharField(max_length=14, min_length=8, write_only=True, required=False)
    token = serializers.CharField(max_length=255, required=False)
    rating = serializers.FloatField(required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'rating', 'token')

    def get_user(self, userId):
        try:
            user = User.objects.get(pk=userId)
            return user
        except User.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})

    def validate(self, data, *args, **kwargs):
        request = self.context.get("request")
        token = decode_header(request=request)
        user = self.get_user(token['id'])
        return user

class UserUpdateSerializer(SetCustomErrorMessagesMixin, serializers.HyperlinkedModelSerializer):
    username = serializers.CharField(max_length=50, required=False)
    email = serializers.EmailField(max_length=50, required=False)
    password = serializers.CharField(max_length=30, min_length=8, write_only=True, required=False)
    newPassword = serializers.CharField(max_length=30, min_length=8, write_only=True, required=False)
    rating = serializers.FloatField(required=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'newPassword', 'rating')
        custom_error_messages = {
            'username': {
                UniqueValidator: _('This username is already taken. Please, try again'),
                RegexValidator: _('Invalid username'),
                MaxLengthValidator: _('Username must be no more than 50 symbols')
            },
            'email': {
                UniqueValidator: _('This email is already taken. Please, try again'),
                EmailValidator: _('Invalid email'),
                MaxLengthValidator: _('Email must be no more than 50 symbols')
            },
            'password': {
                MaxLengthValidator: _('Password must be no more than 30 symbols'),
                MinLengthValidator: _('Password must be more than 8 symbols'),
            },
            'newPassword': {
                MaxLengthValidator: _('Password must be no more than 30 symbols'),
                MinLengthValidator: _('Password must be more than 8 symbols'),
            },
        }

    def get_user(self, *args, **kwargs):
        request = self.context.get("request")
        token = decode_header(request=request)
        try:
            user = User.objects.get(pk=token['id'])
            return user
        except User.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})

    def validate(self, data, *args, **kwargs):
        username = data.get('username', None)
        email = data.get('email', None)
        password = data.get('password', None)
        newPassword = data.get('newPassword', None)

        user = self.get_user()

        if username:
            filteredByUsername = User.objects.filter(username=username)
            if filteredByUsername:
                raise serializers.ValidationError({'username': 'This username is already taken. Please, try again!'})
        if email:
            filteredByEmail = User.objects.filter(email=email)
            if filteredByEmail:
                raise serializers.ValidationError({'email': 'This email is already taken. Please, try again'})
        if password and newPassword:
            if not user.check_password(password):
                raise serializers.ValidationError({'password': 'Invalid password!'})
        return data

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        if validated_data.get('newPassword'):
            instance.set_password(validated_data.get('newPassword'))

        if validated_data.get('rating') == 1:
            instance.rating = instance.rating + 0.1
        if validated_data.get('rating') == 0:
            instance.rating = instance.rating - 0.1
        instance.save()
        return instance
