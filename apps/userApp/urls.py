from django.conf.urls import url
from .views import register_user, current_user, login_user, logout_user, update_user
from .views import PopularUsers

urlpatterns = [
    url(r'^users/current', current_user, name='CurrentUser'),
    url(r'^users/register', register_user, name='CreateUser'),
    url(r'^users/login', login_user, name='LoginUser'),
    url(r'^users/logout', logout_user, name='LogoutUser'),
    url(r'^users/update', update_user, name='UpdateUser'),
    url(r'^users/popular$', PopularUsers.as_view(), name='GetPopularUsers'),  
]
