import jwt

from django.conf import settings

from rest_framework.views import exception_handler
from rest_framework import authentication, exceptions

def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    return response

def decode_header(request):
    authHeader = authentication.get_authorization_header(request).split()
    try:
        token = authHeader[1].decode('utf-8')
        decodedToken = jwt.decode(token, settings.SECRET_KEY)
        return decodedToken
    except IndexError:
        raise exceptions.ParseError({'status': 'fail', 'data': 'Add Bearer!'})
    