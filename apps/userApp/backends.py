import jwt
from django.conf import settings
from rest_framework import authentication, exceptions

from userApp.models import User


class JWTAuthentication():
    authentication_header_prefix = 'Bearer'

    def authenticate(self, request):
        authHeader = authentication.get_authorization_header(request).split()
        authHeaderPrefix = self.authentication_header_prefix.lower()

        if not authHeader:
            return None

        if len(authHeader) == 1:
            return None

        elif len(authHeader) > 2:
            return None

        prefix = authHeader[0].decode('utf-8')
        token = authHeader[1].decode('utf-8')
        

        if prefix.lower() != authHeaderPrefix:
            return None

        return self._authenticate_credentials(request, token)

    def _authenticate_credentials(self, request, token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
        except:
            msg = 'Invalid authentication. Could not decode token.'
            raise exceptions.APIException({'status': 'fail', 'data': msg})

        try:
            user = User.objects.get(pk=payload['id'])
        except User.DoesNotExist:
            msg = 'No user matching this token was found.'
            raise exceptions.APIException({'status': 'fail', 'data': msg})
        
        return (user, token)
