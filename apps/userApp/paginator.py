from rest_framework.response import Response
from rest_framework import pagination
from noteApp.serializers import NoteSerializer

class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        ser = NoteSerializer(context={'request': self.request})
        ser.check_token()
        return Response({
            'status': 'success',
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'data': data
        })
        