from rest_framework import status, generics
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.utils.decorators import method_decorator
from django_filters.rest_framework import DjangoFilterBackend

from folderApp.models import Folder

from .paginator import CustomPagination
from .models import User
from .serializers import LoginSerializer, RegistrationSerializer, UserSerializer, UserUpdateSerializer

param = openapi.Parameter('Authorization', openapi.IN_HEADER, description="User token", type=openapi.TYPE_STRING)

@swagger_auto_schema(operation_description="Register new user", request_body=RegistrationSerializer, method='post')
@api_view(['POST'])
@permission_classes([AllowAny])
def register_user(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = User.objects.get(username=serializer.data['username'])
        if user:
            ser = UserSerializer(user)
            folder = Folder(name='Recently Deleted',
                            readonly=True, userId=ser.data['id'])
            folder.save()

            return Response({
                'status': 'success',
                'data': ser.data
            }, status=status.HTTP_201_CREATED)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Check current user status", manual_parameters=[param], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def current_user(request):
    if request.method == 'GET':
        serializer = UserSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        return Response({
            "status": 'success',
            "data": serializer.data
        }, status=status.HTTP_200_OK)
    
    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Login user", request_body=LoginSerializer, method='post')
@api_view(['POST'])
@permission_classes([AllowAny])
def login_user(request):
    if request.method == 'POST':
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({
            'status': 'success',
            'data': serializer.data,
        }, status=status.HTTP_200_OK)
    
    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Logout user", manual_parameters=[param], method='post')
@api_view(['POST'])
@permission_classes([AllowAny])
def logout_user(request):
    if request.method == 'POST':
        serializer = UserSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        return Response({
            'status': 'success',
            'data': 'Logout success',
        }, status=status.HTTP_200_OK)
    
    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Update user", manual_parameters=[param], request_body=UserUpdateSerializer, method='put')
@api_view(['PUT'])
@permission_classes([AllowAny])
def update_user(request):
    if request.method == 'PUT':
        userSer = UserUpdateSerializer(data=request.data, context={'request': request})
        user = userSer.get_user()
        userSer.is_valid(raise_exception=True)
        updateUserSer = UserUpdateSerializer()
        updateUserSer.update(user, userSer.validated_data)
        return Response({
            'status': 'success',
            'data': userSer.data
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description="Get popular Users by rating"))
class PopularUsers(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [OrderingFilter, DjangoFilterBackend]
    pagination_class = CustomPagination
    ordering = ['rating']
