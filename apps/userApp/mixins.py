

class SetCustomErrorMessagesMixin:
    def __init__(self, *args, **kwargs):
        # noinspection PyArgumentList
        super(SetCustomErrorMessagesMixin, self).__init__(*args, **kwargs)
        self.replace_validators_messages()

    def replace_validators_messages(self):
        for fieldName, validatorsLookup in self.custom_error_messages.items():
            # noinspection PyUnresolvedReferences
            for validator in self.fields[fieldName].validators:
                if type(validator) in validatorsLookup:
                    validator.message = validatorsLookup[type(validator)]

    @property
    def custom_error_messages(self):
        meta = getattr(self, 'Meta', None)
        return getattr(meta, 'custom_error_messages', {})
