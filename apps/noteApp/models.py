from django.db import models

from userApp.models import User

class Note(models.Model):
    name = models.CharField(max_length=30)
    content = models.TextField(max_length=35000)
    locked = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    folderId = models.IntegerField(null=False)
    userId = models.IntegerField(null=False)
    rating = models.IntegerField(default=0, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=0)
    createDt = models.DateTimeField(auto_now_add=True)
    updateDt = models.DateTimeField(default=None, null=True)

    def __str__(self):
        return str(self.name)

class Like(models.Model):
    userId = models.IntegerField(null=False)
    noteId = models.IntegerField(null=False)
    createDt = models.DateTimeField(auto_now_add=True)
