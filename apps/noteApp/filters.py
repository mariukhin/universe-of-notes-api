import django_filters
from userApp.models import User

from .models import Note

class NoteFilter(django_filters.FilterSet):
    author = django_filters.ModelChoiceFilter(field_name='author__username', to_field_name='username', queryset=User.objects.all())
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Note
        fields = ['author', 'folderId', 'name']
