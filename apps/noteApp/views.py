from rest_framework import status, generics
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.utils.decorators import method_decorator
from django_filters.rest_framework import DjangoFilterBackend

from userApp.paginator import CustomPagination
from userApp.serializers import UserUpdateSerializer, UserSerializer

from .models import Note, Like
from .serializers import NoteSerializer, LikeSerializer
from .filters import NoteFilter

param = openapi.Parameter('Authorization', openapi.IN_HEADER, description="User token", type=openapi.TYPE_STRING)
folderIdParam = openapi.Parameter('folderId', openapi.IN_QUERY, description="Folder ID", type=openapi.TYPE_INTEGER)
nameParam = openapi.Parameter('name', openapi.IN_QUERY, description="Filter word", type=openapi.TYPE_STRING)
noteIdsParam = openapi.Parameter('noteIds', openapi.IN_QUERY, description="Delete notes by Ids", type=openapi.TYPE_STRING)
orderingParam = openapi.Parameter('ordering', openapi.IN_QUERY, description="Delete notes by Ids", type=openapi.TYPE_STRING)

@swagger_auto_schema(operation_description="Get list of Notes by folderId", manual_parameters=[param, folderIdParam], method='get')
@swagger_auto_schema(operation_description="Create new Note", request_body=NoteSerializer, manual_parameters=[param], method='post')
@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def note_get_create(request):
    ser = NoteSerializer(context={'request': request})
    ser.check_token()

    if request.method == 'GET':
        folderId = request.GET.get('folderId', None)
        getNotesByFolderId = Note.objects.filter(folderId=folderId)
        activeNotes = getNotesByFolderId.filter(active=True)
        ser = NoteSerializer(activeNotes, many=True)
        return Response({
            'status': 'success',
            'data': ser.data
        }, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        serializer = NoteSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            filteredByFolderIdAndName = Note.objects.filter(
                folderId=serializer.validated_data['folderId']).filter(name=serializer.validated_data['name'])
            if filteredByFolderIdAndName:
                return Response({
                    'status': 'fail',
                    'data': 'A note with this name has already been created in this folder!',
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                serializer.save()

        note = Note.objects.filter(folderId=serializer.data['folderId']).filter(
            name=serializer.data['name'])
        if note:
            ser = NoteSerializer(note[0])
            return Response({
                'status': 'success',
                'data': ser.data
            }, status=status.HTTP_201_CREATED)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description="Get notes by folderId or author or noteName"
))
class GetNotes(generics.ListAPIView):
    author = UserSerializer
    queryset = Note.objects.filter(active=True, locked=False)
    serializer_class = NoteSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ('folderId', 'author', 'name')
    filter_class = NoteFilter
    pagination_class = CustomPagination

@swagger_auto_schema(operation_description="Get Note by Id", manual_parameters=[param], method='get')
@swagger_auto_schema(operation_description="Edit Note", request_body=NoteSerializer, manual_parameters=[param], method='put')
@api_view(['GET', 'PUT'])
@permission_classes([AllowAny])
def note_get_put(request, pk):
    ser = NoteSerializer(context={'request': request, 'pk': pk})
    ser.check_token()
    note = ser.get_note_by_pk()

    if request.method == 'GET':
        noteSer = NoteSerializer(note)
        return Response({
            'status': 'success',
            'data': noteSer.data,
        }, status=status.HTTP_200_OK)

    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = NoteSerializer(note, data=data)
        if serializer.is_valid(raise_exception=True):
            serNote = NoteSerializer(note)
            filteredByFolderId = Note.objects.filter(
                folderId=serNote.data['folderId'])
            if 'name' in data:
                filteredByName = filteredByFolderId.filter(
                    name=serializer.validated_data['name'])
                if filteredByName:
                    return Response({
                        'status': 'fail',
                        'data': 'A note with this name has already been created in this folder!',
                    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            serializer.save()
            return Response({
                'status': 'success',
                'data': serializer.data,
            }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Filter notes", manual_parameters=[param, nameParam, folderIdParam], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def note_filter(request):
    ser = NoteSerializer(context={'request': request})
    ser.check_token()

    if request.method == 'GET':
        folderId = request.GET.get('folderId', None)
        notes = Note.objects.filter(folderId=folderId)
        activeNotes = notes.filter(active=True)
        name = request.GET.get('name', None)

        if name is not None:
            activeNotes = activeNotes.filter(name__icontains=name)

        serializer = NoteSerializer(activeNotes, many=True)
        return Response({
            'status': 'success',
            'data': serializer.data,
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@swagger_auto_schema(operation_description="Get recently deleted Notes", manual_parameters=[param], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def note_recently_deleted(request):
    ser = NoteSerializer(context={'request': request})
    userId = ser.check_token()

    if request.method == 'GET':
        filteredByUserId = Note.objects.filter(userId=userId)
        notes = filteredByUserId.filter(active=False)
        serializer = NoteSerializer(notes, many=True)
        return Response({
            'status': 'success',
            'data': serializer.data,
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Activate Note", manual_parameters=[param], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def note_activate(request, pk):
    ser = NoteSerializer(context={'request': request, 'pk': pk})
    ser.check_token()
    note = ser.get_note_by_pk()

    if request.method == 'GET':
        serNote = NoteSerializer(note)
        serNote.set_active(note, active=True)
        return Response({
            'status': 'success'
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Deactivate Note", manual_parameters=[param], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def note_deactivate(request, pk):
    ser = NoteSerializer(context={'request': request, 'pk': pk})
    ser.check_token()
    note = ser.get_note_by_pk()

    if request.method == 'GET':
        serNote = NoteSerializer()
        serNote.set_active(note, active=False)
        return Response({
            'status': 'success',
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
@swagger_auto_schema(operation_description="Delete Notes", manual_parameters=[param, noteIdsParam], method='delete')
@api_view(['DELETE'])
@permission_classes([AllowAny])
def note_delete(request):
    ser = NoteSerializer(context={'request': request})
    ser.check_token()

    if request.method == 'DELETE':
        noteIds = request.GET.get('noteIds', None).split(',')
        for num in noteIds:
            num = int(num)
            ser = NoteSerializer(context={'pk': num})
            note = ser.get_note_by_pk()
            note.delete()
        
        return Response({
            'status': 'success'
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Update Note rating", request_body=NoteSerializer, manual_parameters=[param], method='put')
@api_view(['PUT'])
@permission_classes([AllowAny])
def note_update_rating(request, pk):
    ser = NoteSerializer(context={'request': request, 'pk': pk})
    userId = ser.check_token()
    note = ser.get_note_by_pk()

    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = NoteSerializer(note, data=data)
        serLike = LikeSerializer(data={'userId': userId, 'noteId': pk})
        userSer = UserUpdateSerializer(data=data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            like = Like.objects.filter(userId=userId).filter(noteId=pk)
            serNote = NoteSerializer(note)
            user = userSer.get_user()
            userSer.is_valid(raise_exception=True)
            updateUserSer = UserUpdateSerializer()
            if data['rating'] == 1:
                if like:
                    return Response({
                        'status': 'fail',
                        'data': 'You can`t like it more than one time!'
                    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    serNote.set_rating(note, rating=note.rating + 1)
                    serLike.is_valid()
                    serLike.save()
                    #Update user rating
                    updateUserSer.update(user, userSer.validated_data)
            elif data['rating'] == 0:
                if like:
                    serNote.set_rating(note, rating=note.rating - 1)
                    #Update user rating
                    updateUserSer.update(user, userSer.validated_data)
                    like.delete()
                else:
                    return Response({
                        'status': 'fail',
                        'data': 'Don`t have this like'
                    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response({
                    'status': 'fail',
                    'data': 'Invalid rating. Should be 1 or 0'
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            return Response({
                'status': 'success',
                'data': serializer.data
            }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description="Get popular Notes by rating"))
class PopularNotes(generics.ListAPIView):
    queryset = Note.objects.filter(locked=False, active=True)
    serializer_class = NoteSerializer
    filter_backends = [OrderingFilter, DjangoFilterBackend]
    pagination_class = CustomPagination
    ordering = ['rating']

@swagger_auto_schema(operation_description="Get Likes by Current UserId", manual_parameters=[param], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def like_list(request):
    ser = LikeSerializer(context={'request': request})
    userId = ser.check_token()

    if request.method == 'GET':
        likes = Like.objects.filter(userId=userId)
        serializer = LikeSerializer(likes, many=True)
        return Response({
            'status': 'success',
            'data': serializer.data,
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
