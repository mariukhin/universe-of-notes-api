from datetime import datetime
from django.utils.timezone import get_current_timezone
from django.core.validators import MaxLengthValidator
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions

from userApp.mixins import SetCustomErrorMessagesMixin
from userApp.models import User
from userApp.utils import decode_header

from .models import Note, Like

class NoteSerializer(SetCustomErrorMessagesMixin, serializers.ModelSerializer):
    name = serializers.CharField(required=False, max_length=30)
    content = serializers.CharField(required=False)
    locked = serializers.BooleanField(required=False)
    active = serializers.BooleanField(required=False)
    folderId = serializers.IntegerField(required=False)
    userId = serializers.IntegerField(required=False)
    rating = serializers.IntegerField(required=False)
    createDt = serializers.DateTimeField(required=False)
    updateDt = serializers.DateTimeField(required=False)
    author = serializers.StringRelatedField(required=False)

    class Meta:
        model = Note
        fields = ('id', 'name', 'content', 'locked', 'active',
                  'folderId', 'userId', 'rating', 'createDt', 'updateDt', 'author')
        custom_error_messages = {
            'name': {
                MaxLengthValidator: _('Note name must be no more than 30 symbols'),
            },
        }  

    def create(self, validated_data):
        user = User.objects.get(id=validated_data['userId'])
        return Note.objects.create(**validated_data, author=user)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.content = validated_data.get('content', instance.content)
        instance.locked = validated_data.get('locked', instance.locked)
        instance.updateDt = datetime.now(tz=get_current_timezone())
        instance.save()
        return instance

    def get(self, instance):
        return instance

    def check_token(self):
        request = self.context.get("request")
        token = decode_header(request=request)
        try:
            user = User.objects.get(pk=token['id'])
            return user.id
        except User.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})

    def get_note_by_pk(self):
        pk = self.context.get("pk")
        try:
            note = Note.objects.get(pk=pk)
            return note
        except Note.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})

    def set_active(self, instance, active):
        instance.active = active
        instance.save()

    def set_rating(self, instance, rating):
        instance.rating = rating
        instance.save()

    def set_author(self, instance, userId):
        user = User.objects.get(id=userId)
        instance.author = user
        instance.save()

class LikeSerializer(SetCustomErrorMessagesMixin, serializers.ModelSerializer):
    userId = serializers.IntegerField(required=False)
    noteId = serializers.IntegerField(required=False)
    createDt = serializers.DateTimeField(required=False)

    class Meta:
        model = Note
        fields = ('id', 'userId', 'noteId', 'createDt') 

    def create(self, validated_data):
        return Like.objects.create(**validated_data)
        
    def check_token(self):
        request = self.context.get("request")
        token = decode_header(request=request)
        try:
            user = User.objects.get(pk=token['id'])
            return user.id
        except User.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})
    