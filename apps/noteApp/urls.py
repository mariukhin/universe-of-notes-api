from django.conf.urls import url
from .views import note_get_create, note_get_put, note_filter, note_recently_deleted, note_activate, note_delete, note_deactivate, note_update_rating
from .views import PopularNotes, GetNotes, like_list

urlpatterns = [
    url(r'^notes$', note_get_create, name='GetCreateNote'),
    url(r'^notes/list$', GetNotes.as_view(), name='GetNotesList'),
    url(r'^notes/(?P<pk>[0-9]+)$', note_get_put, name='GetEditNote'),
    url(r'^notes/filter$', note_filter, name='FilterNote'),
    url(r'^notes/deleted', note_recently_deleted, name='GetRecentlyDeletedNotes'),
    url(r'^notes/(?P<pk>[0-9]+)/activate', note_activate, name='ActivateNote'),
    url(r'^notes/(?P<pk>[0-9]+)/deactivate', note_deactivate, name='DeactivateNote'),
    url(r'^notes/delete$', note_delete, name='DeleteNote'), 
    url(r'^notes/(?P<pk>[0-9]+)/rating$', note_update_rating, name='UpdateRatingNote'),
    url(r'^notes/popular$', PopularNotes.as_view(), name='GetPopularNotes'),
    url(r'^likes/list$', like_list, name='GetLikesByCurrentUserId'),  

]