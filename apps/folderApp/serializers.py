from django.core.validators import RegexValidator, MaxLengthValidator
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions
from userApp.models import User
from userApp.utils import decode_header
from noteApp.models import Note

from .models import Folder


class FolderSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    readonly = serializers.BooleanField(required=False)
    userId = serializers.IntegerField(required=False)

    class Meta:
        model = Folder
        fields = ('id', 'name', 'readonly', 'userId')
        custom_error_messages = {
            'name': {
                MaxLengthValidator: _('Folder name must be no more than 30 symbols')
            },
            'userId': {
                RegexValidator: _('Enter number!')
            }
        }    
    
    def create(self, validated_data):
        return Folder.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.readonly = validated_data.get('readonly', instance.readonly)
        instance.save()
        return instance

    def deleteNotes(self, instance):
        notes = Note.objects.filter(folderId=instance.id)
        for note in notes:
            note.delete()
        instance.delete()
        
    def get_userId(self):
        request = self.context.get("request")
        token = decode_header(request=request)
        try:
            user = User.objects.get(pk=token['id'])
            return user.id
        except User.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})

    def get_folder_by_pk(self):
        pk = self.context.get("pk")
        try:
            folder = Folder.objects.get(pk=pk)
            return folder
        except Folder.DoesNotExist:
            raise exceptions.NotFound({'status': 'fail', 'data': 'Not found!'})
