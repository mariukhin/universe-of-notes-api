from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from .models import Folder
from .serializers import FolderSerializer

param = openapi.Parameter('Authorization', openapi.IN_HEADER, description="User token", type=openapi.TYPE_STRING)
filterParam = openapi.Parameter('name', openapi.IN_QUERY, description="Filter by name", type=openapi.TYPE_STRING)

@swagger_auto_schema(operation_description="Get list of Folders by userId", manual_parameters=[param], method='get')
@swagger_auto_schema(operation_description="Create new folder", request_body=FolderSerializer, manual_parameters=[param], method='post')
@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def folder_get_create(request):
    serializer = FolderSerializer(data=request.data, context={'request': request})
    userId = serializer.get_userId()

    if request.method == 'GET':
        getFoldersByUserId = Folder.objects.filter(userId=userId)
        ser = FolderSerializer(getFoldersByUserId, many=True)
        return Response({
            'status': 'success',
            'data': ser.data
        }, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        if serializer.is_valid(raise_exception=True):
            filteredByUserIdAndName = Folder.objects.filter(
                userId=userId).filter(name=serializer.validated_data['name'])
            if filteredByUserIdAndName:
                return Response({
                    'status': 'fail',
                    'data': 'A folder with this name has already been created!',
                }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            serializer.save()

        folder = Folder.objects.filter(userId=userId).filter(
            name=serializer.data['name'])
        if folder:
            ser = FolderSerializer(folder[0])
            return Response({
                'status': 'success',
                'data': ser.data
            }, status=status.HTTP_201_CREATED)
    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Get Folder by primary key", manual_parameters=[param], method='get')
@swagger_auto_schema(operation_description="Edit folder", request_body=FolderSerializer, manual_parameters=[param], method='put')
@swagger_auto_schema(operation_description="Delete folder", manual_parameters=[param], method='delete')
@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([AllowAny])
def folder_get_put_delete(request, pk):
    folderSer = FolderSerializer(data=request.data, context={'request': request, 'pk': pk})
    folder = folderSer.get_folder_by_pk()
    userId = folderSer.get_userId()

    if request.method == 'GET':
        allUserFolders = Folder.objects.filter(userId=userId)
        currentFolder = allUserFolders.filter(id=folder.id)
        if currentFolder:
            folderSer = FolderSerializer(currentFolder[0])
            return Response({
                'status': 'success',
                'data': folderSer.data,
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                'status': 'fail',
                'data': 'Not current user folder!',
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    elif request.method == 'PUT':
        serializer = FolderSerializer(folder, data=request.data)
        if serializer.is_valid(raise_exception=True):
            filteredByUserId = Folder.objects.filter(userId=userId)
            if 'name' in request.data:
                filteredByName = filteredByUserId.filter(
                    name=serializer.validated_data['name'])
                if filteredByName:
                    return Response({
                        'status': 'fail',
                        'data': 'A folder with this name has already been created!',
                    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            serializer.save()
            return Response({
                'status': 'success',
                'data': serializer.data,
            }, status=status.HTTP_200_OK)

    elif request.method == 'DELETE':
        folderSer.deleteNotes(folder)
        return Response({
            'status': 'success',
        }, status=status.HTTP_200_OK)

    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(operation_description="Filter folder", manual_parameters=[param, filterParam], method='get')
@api_view(['GET'])
@permission_classes([AllowAny])
def folder_filter(request):
    if request.method == 'GET':
        ser = FolderSerializer(context={'request': request})
        userId = ser.get_userId()
        folders = Folder.objects.filter(userId=userId)
        name = request.GET.get('name', None)

        if name is not None:
            folders = folders.filter(name__icontains=name)

        serializer = FolderSerializer(folders, many=True)
        return Response({
            'status': 'success',
            'data': serializer.data,
        }, status=status.HTTP_200_OK)
    
    return Response({
        'status': 'fail',
        'data': 'Don`t have this request method'
    }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
