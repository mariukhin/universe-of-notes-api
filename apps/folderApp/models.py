from django.db import models

class Folder(models.Model):
    name = models.CharField(max_length=30)
    readonly = models.BooleanField(default=False)
    userId = models.IntegerField()

    def __str__(self):
        return str(self.name)
        