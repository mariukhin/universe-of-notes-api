from django.conf.urls import url
from .views import folder_get_create, folder_get_put_delete, folder_filter

urlpatterns = [
    url(r'^folders$', folder_get_create, name='GetCreateFolder'),
    url(r'^folders/(?P<pk>[0-9]+)$', folder_get_put_delete, name='GetEditDeleteFolder'),
    url(r'^folders/filter$', folder_filter, name='FilterFolder')
]
