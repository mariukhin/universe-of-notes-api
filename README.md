# universe-of-notes-api
API for Universe of notes using Django framework + MongoDB database

You need to have installed python(version not lower than 3.8) and pip

# Run API

1. Install [Python](https://www.python.org/downloads/)
2. Install [PIP](https://pythonru.com/baza-znanij/ustanovka-pip-dlja-python-i-bazovye-komandy) package manager for Python
3. Run:
```bash
pip3 install -r requirements.txt
```
4. After that, run:
```bash
python3 manage.py runserver
```
5. Enjoy API :)

# API commands

## Run server
```bash
python3 manage.py runserver
```
## Check code using PyLint
```bash
pylint apps/
```
## Make migrations 
```bash
python3 manage.py makemigrations
```
## Migrate changes to DB
```bash
python3 manage.py migrate
```

# Authors

[Maksym Mariukhin](https://www.linkedin.com/in/maksym-mariukhin/)

[Maksym Chernenko](https://www.linkedin.com/in/maksym-chernenko/)
